import { Flex } from '@chakra-ui/react'
import Web3Wallet from '../../components/Web3Wallet'

const AppLayout = ({ children }) => {
  return (
    <Flex minHeight="100vh" width="100vw" maxWidth="100vw" direction="column">
      <Flex justify='center' p={4}><Web3Wallet /></Flex>
      {children}
    </Flex>
  )
}

export default AppLayout
