import { ethers } from 'ethers'
import { useContext, useEffect, useState } from "react"
import Web3Modal from "web3modal"

import context from '../context/web3Context'

const useWeb3 = () => {
  const { state: { signer }, dispatch } = useContext(context)

  const [walletAddress, setWalletAddress] = useState<string>('')

  const connect = async () => {
    const web3Modal = new Web3Modal({ network: 'localhost' })

    const connection = await web3Modal.connect()
    const provider = new ethers.providers.Web3Provider(connection)
    const signer = provider.getSigner()

    dispatch({ type: 'setSigner', value: signer })
    setWalletAddress(await signer.getAddress())
  }

  const signMessage = async (message: string) => {
    return await signer.signMessage(message)
  }

  useEffect(() => {
    (async () => setWalletAddress(await signer?.getAddress()))()
  }, [signer])

  return { connect, signer, signMessage, walletAddress }
}

export default useWeb3
