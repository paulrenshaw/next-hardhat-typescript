import { Flex, Heading } from '@chakra-ui/react'

const title = process.env.NEXT_PUBLIC_SITE_TITLE

const HomePage = () => {
  return (
    <Flex direction='column'>
      <Heading as='h1' textAlign='center' m={4}>{title}</Heading>
    </Flex>
  )
}

export default HomePage
