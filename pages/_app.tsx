import { ChakraProvider } from "@chakra-ui/react"
import { AppProps } from 'next/app'
import Head from 'next/head'

import AppLayout from "../layouts/AppLayout"
import Web3Provider from "../context/Web3Provider"

import '../styles/global.css'

const title = process.env.NEXT_PUBLIC_SITE_TITLE

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <ChakraProvider>
      <Web3Provider>
        <Head>
          <title>{title}</title>
        </Head>
        <AppLayout>
          <Component {...pageProps} />
        </AppLayout>
      </Web3Provider>
    </ChakraProvider>
  )
}

export default App
