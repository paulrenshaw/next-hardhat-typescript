const hre = require("hardhat");
require('dotenv').config({ path: '../.env.local' })

async function main() {
  const Token = await hre.ethers.getContractFactory("Token");

  const name = 'Token'
  const symbol = 'TKN'
  const proxyRegistryAddress = process.env.MAINNET_ACCOUNT_PUBLIC_ADDRESS
  const tokenPrice = hre.ethers.utils.parseEther('0.001')

  const token = await Token.deploy(
    name,
    symbol,
    proxyRegistryAddress,
    tokenPrice
  );

  await token.deployed();
  console.log(`${name} deployed to:`, token.address);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
