import { Box, Button } from "@chakra-ui/react"
import useWeb3 from "../hooks/useWeb3"

const Web3Wallet = () => {
  const { connect, walletAddress } = useWeb3()

  return (
    <>
      {!!walletAddress
        ? <Box>Connected to address: {walletAddress.substring(0, 6)}...{walletAddress.substring(walletAddress.length - 4)}</Box>
        : <Button onClick={connect}>Connect</Button>}
    </>
  )
}

export default Web3Wallet
