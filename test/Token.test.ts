import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers'
import { ethers } from 'hardhat'
import { Token } from '../typechain'

let token: Token
let owner: SignerWithAddress

describe("Token", () => {
  beforeEach(async () => {
    const Token = await ethers.getContractFactory("Token")
    owner = (await ethers.getSigners())[0]

    token = await Token.connect(owner).deploy(
      'Token',
      'TKN',
      owner.address,
      ethers.utils.parseEther('0.001')
    )
    await token.deployed()
  })

  it("should allow to purchase tokens", async () => {
    const value = ethers.utils.parseUnits('0.001', 'ether')
    await expect(token.connect(owner).mint({ value })).resolves.not.toThrow()
  })
})
