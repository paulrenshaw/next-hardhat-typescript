// SPDX-License-Identifier: MIT OR Apache-2.0
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

import "./ContentMixin.sol";
import "./NativeMetaTransaction.sol";

import "hardhat/console.sol";

contract OwnableDelegateProxy {}

contract ProxyRegistry {
    mapping(address => OwnableDelegateProxy) public proxies;
}

contract Token is ContextMixin, ERC721Enumerable, NativeMetaTransaction, Ownable {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;

    address _proxyRegistryAddress;
    uint256 _tokenPrice;

    constructor(
        string memory name,
        string memory symbol,
        address proxyRegistryAddress,
        uint256 tokenPrice
    ) ERC721(name, symbol) {
        _proxyRegistryAddress = proxyRegistryAddress;
        _tokenPrice = tokenPrice;
    }

    function mint() public payable {
        require(msg.value >= _tokenPrice, "not enough ETH sent");

        _tokenIds.increment();
        uint256 newTokenId = _tokenIds.current();
        _mint(msg.sender, newTokenId);
    }

    function withdraw() public onlyOwner {
        payable(owner()).transfer(address(this).balance);
    }

    /**
     * OPENSEA Stuff
     */

    /**
     * Override isApprovedForAll to whitelist user's OpenSea proxy accounts to enable gas-less listings.
     */
    function isApprovedForAll(address owner, address operator)
        override
        public
        view
        returns (bool)
    {
        // Whitelist OpenSea proxy contract for easy trading.
        ProxyRegistry proxyRegistry = ProxyRegistry(_proxyRegistryAddress);
        if (address(proxyRegistry.proxies(owner)) == operator) {
            return true;
        }

        return super.isApprovedForAll(owner, operator);
    }

    /**
     * This is used instead of msg.sender as transactions won't be sent by the original token owner, but by OpenSea.
     */
    function _msgSender()
        internal
        override
        view
        returns (address sender)
    {
        return ContextMixin.msgSender();
    }
}
