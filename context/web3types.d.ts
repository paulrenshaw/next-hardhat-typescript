import { ethers } from "ethers";

interface Web3State {
  signer: ethers.providers.JsonRpcSigner
}

interface Web3Action {
  type: string,
  value: any
}
