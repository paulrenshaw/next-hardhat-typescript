# Next Hardhat TypeScript

Boilerplate web app and NFT smart contract built with:

- ChakraUI
- Ethers
- Hardhat + Typechain
- Jest
- Next.js
- OpenZeppelin
- TypeScript

Demo at https://next-hardhat-typescript.vercel.app/

Has a basic Web3Provider component to use the connected signer throughout the application via the useWeb3 custom React hook - an example of usage is the Web3Wallet component used to connect a wallet and display the connected address.

Everything is configured to use typescript by default.

## Installation
```
yarn

hardhat compile
```

## Running Locally
```
yarn dev
```

## Test
```
jest
```
